﻿using Android.App;
using Android.Widget;
using Android.OS;

namespace xamarinandroidcisample
{
    [Activity(Label = "xamarinandroidcisample", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        int count = 1;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button button = FindViewById<Button>(Resource.Id.myButton);

            button.Click += delegate {
                CheckBox cbx = FindViewById<CheckBox>(Resource.Id.checkBox1);
                count = count + ((cbx.Checked) ? 2 : 1);
                button.Text =  $"{count} clicks!"; 
            };
        }
    }
}

